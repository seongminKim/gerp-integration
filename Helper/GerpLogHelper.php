<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-22
 * Time: 오후 6:43
 */

namespace Eguana\GERP\Helper;

use Eguana\GERP\Model\ResourceModel\Log\Collection;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;

class GerpLogHelper extends AbstractHelper
{
    protected $_logCollection;

    public function __construct(
        Context $context,
        Collection $logCollection
    )
    {
        $this->_logCollection = $logCollection;
        parent::__construct($context);
    }


    public function multipleInsert($data){

        $this->_getConnection()->insertArray(
            $this->_getTableName(),
            [
                'gerp_flag',
                'status',
                'identifier_name',
                'identifier',
                'sub_identifier_name',
                'sub_identifier',
                'website_code',
                'interface_id',
                'sender_id',
                'receiver_id',
                'gerp_data',
                'message'
            ],$data
        );
    }

    protected function _getTableName(){
        return $this->getResource()->getMainTable();
    }

    protected function _getConnection(){
        return $this->_logCollection->getConnection();
    }


    protected function getResource(){
        return $this->_logCollection->getResource();
    }

}