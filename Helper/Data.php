<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-05
 * Time: 오후 5:10
 */

namespace Eguana\GERP\Helper;


use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\StoreManagerInterface;

class Data extends AbstractHelper
{
    const GERP_INTERFACE_CONFIG_KEY = 'gerp_interface_list';

    const GERP_SKU_SPLIT = '.';

    protected $_gerpConfig;

    protected $_storeManager;

    protected $_websiteData;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Eguana\GERP\Model\Config\Data $gerpConfig,
        StoreManagerInterface $storeManager
    )
    {
        $this->_gerpConfig = $gerpConfig;
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    public function getGerpWebsiteData(){

        if(is_null($this->_websiteData)){
            $webSiteData = [];
            $websites = $this->_storeManager->getWebsites();

            foreach ($websites as $website){
                $affiliateCode = $this->getGerpGeneralConfig('affiliate_code',$website->getId());
                $webSiteData[$affiliateCode] = [
                    'website_code' => $website->getCode(),
                    'website_id' => $website->getId(),
                    'store_id' => $website->getDefaultStore()->getId(),
                    'affiliate_code' => $affiliateCode,
                    'sku_prefix' => $this->getGerpGeneralConfig('sku_prefix',$website->getId()),
                    'legal_entity_name' => $this->getGerpGeneralConfig('legal_entity_name',$website->getId()),
                    'obs_id' => $this->getGerpGeneralConfig('obs_id',$website->getId()),
                    'gerp_id' => $this->getGerpGeneralConfig('gerp_id',$website->getId()),
                    'bill_to_code' => $this->getGerpGeneralConfig('bill_to_code',$website->getId()),
                    'ship_to_code' => $this->getGerpGeneralConfig('ship_to_code',$website->getId()),
                    'order_type' => $this->getGerpGeneralConfig('order_type',$website->getId()),
                    'cancel_type' => $this->getGerpGeneralConfig('cancel_type',$website->getId()),
                    'return_type' => $this->getGerpGeneralConfig('return_type',$website->getId()),
                    'ontime_shop_to_flag' => $this->getGerpGeneralConfig('ontime_shop_to_flag',$website->getId()),
                    'create_by' => $this->getGerpGeneralConfig('create_by',$website->getId()),
                    'order_source_name' => $this->getGerpGeneralConfig('order_source_name',$website->getId()),
                    'partial_flag' => $this->getGerpGeneralConfig('partial_flag',$website->getId()),
                    'calculation_price_flag' => $this->getGerpGeneralConfig('calculation_price_flag',$website->getId()),
                    'uri' => $this->getGerpGeneralConfig('uri',$website->getId()),
                    'sku_split' => self::GERP_SKU_SPLIT,
                    'timezone' => $this->scopeConfig->getValue('general/locale/timezone','website',$website->getId()),
                    'omv_shipping_method_code' => $this->getGerpGeneralConfig('omv_shipping_method_code',$website->getId()),
                ];
            }

            $this->_websiteData = $webSiteData;
        }

        return $this->_websiteData;
    }

    public function getWebsiteTimeZone(){
    }

    public function getGerpProductConfig($path , $scopeValue = 0){
        return $this->getGerpConfig('product/'.$path,$scopeValue);
    }

    public function getGerpOrderConfig($path , $scopeValue = 0){
        return $this->getGerpConfig('order/'.$path,$scopeValue);
    }

    public function getGerpGeneralConfig($path , $scopeValue = 0){
        return $this->getGerpConfig('general/'.$path,$scopeValue);
    }

    public function getGerpConfig($path , $scopeValue = 0){
        return $this->scopeConfig->getValue('gerp/'.$path,'websites',$scopeValue);
    }

    public function getGerpInterfaceConfig(){
        return $this->_gerpConfig->get(self::GERP_INTERFACE_CONFIG_KEY);
    }
}