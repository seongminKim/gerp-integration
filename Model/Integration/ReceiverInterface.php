<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-11
 * Time: 오후 2:39
 */

namespace Eguana\GERP\Model\Integration;


interface ReceiverInterface
{
    const GERP_DATA_COLUMN = 'gerp_data';

    /**
     * @param $bindData array
     * return array
     */
    public function process($bindData);

    /**
     * @param $resultData array
     * @return void
     */
    public function result($resultData);
}