<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-18
 * Time: 오후 12:29
 */

namespace Eguana\GERP\Model\Integration\Order\Omv;

use Eguana\GERP\Model\Integration\Order\AbstractSendOrder;
use Eguana\GERP\Model\Integration\Product\AbstractProduct;
use Eguana\GERP\Model\Source\Status;
use Amasty\MultiInventory\Model\Warehouse\OrderItemRepository;
use Eguana\GERP\Helper\GerpLogHelper;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Model\Order\Address;
use Magento\Sales\Model\Order\ItemRepository;


class Rma extends \Eguana\GERP\Model\Integration\Order\Omd\Rma
{
    protected $_documentType = 'CXML_ACC_ORDREQ';

    protected function getRmaItemCollection(){

        if(!$this->_rmaItemCollection){
            $rmaItemCollection = $this->_createRmaItemCollection();
            $rmaItemCollection->addAttributeToSelect('*');
            $rmaItemCollection->getSelect()->joinLeft(['soi' => 'sales_order_item'],'soi.item_id = order_item_id',['product_from','product_type']);
            $rmaItemCollection->getSelect()->where('product_from = ?',AbstractProduct::OMV_PRODUCT_FROM);
            $rmaItemCollection->getSelect()->where('product_type in (?)', ['simple','bundle']);
            $rmaItemCollection->getSelect()->where('soi.created_at > ?', '2018-12-17 12:00:00');
            $rmaItemCollection->getSelect()->where('e.interface_status is null');
            $rmaItemCollection->getSelect()->where('e.status = ?','authorized');
            $this->_rmaItemCollection = $rmaItemCollection;
        }

        return $this->_rmaItemCollection;
    }

    /**
     * @param $gerpWebsiteData []
     * @param $rma \Magento\Rma\Model\Rma
     * @param $rmaItem \Magento\Rma\Model\Item
     * @param $order \Magento\Sales\Model\Order
     * @param $item OrderItemInterface
     * @param $shippingAddress \Magento\Sales\Model\Order\Address
     * @param $modelCode string
     * @param $wareHouseCode string
     * @param $isPtoMaster boolean
     * @param $parentModelCode string
     * @param $isPtoChild int
     * @return array
     */
    protected function _rmaDataToArray(
        $gerpWebsiteData,
        $rma,
        $rmaItem,
        $order,
        $item,
        $shippingAddress,
        $modelCode,
        $wareHouseCode,
        $isPtoMaster,
        $parentModelCode,
        $isPtoChild
    ){
        $reasonCode = $this->getAllReturnResonCode();
        $externalIncrementId = $this->convertExternalIncrementId($rma->getIncrementId());
        $externalItemId = $this->convertExternalIncrementId($rmaItem->getId());
        $product = $this->_productRepository->get($item->getSku());
        $uomAttribute = $product->getCustomAttribute('primary_uom_code');
        $uomValue = ($uomAttribute)?$uomAttribute->getValue():'';

        return [
            'AFFILIATE_CODE' => $gerpWebsiteData['affiliate_code'],
            'ORDER_TYPE' => 'PC',
            'ORIG_SYS_DOCUMENT_REF' => $externalIncrementId,
            'CUSTOMER_ORDER_DATE' => $this->defaultTimeToStoreTime($rma->getDateRequested(),$gerpWebsiteData['timezone']),
            'CURRENCY_CODE' => $order->getOrderCurrencyCode(),
            'TOT_ORDER_AMT' => $order->getGrandTotal(),
            'SHIPPING_METHOD_CODE' => $gerpWebsiteData['omv_shipping_method_code'],
            'SHIP_TO_CODE' => $gerpWebsiteData['ship_to_code'],
            'BILL_TO_CODE' => $gerpWebsiteData['bill_to_code'],
            'SHIP_TO_ADDRESS_LINE1_INFO' => $shippingAddress->getStreetLine(1),
            'SHIP_TO_ADDRESS_LINE2_INFO' => $shippingAddress->getStreetLine(2).' '.$shippingAddress->getRegion(),
            'SHIP_TO_POSTAL_CODE' => $shippingAddress->getPostcode(),
            'SHIP_TO_COUNTRY_CODE' => strtoupper($shippingAddress->getCountryId()),
            'SHIP_TO_CUSTOMER_NAME' => $shippingAddress->getName(),
            'SHIP_TO_CUSTOMER_EMAIL' => $shippingAddress->getEmail(),
            'SHIP_TO_CUSTOMER_PHONE1' => $shippingAddress->getTelephone(),
            'ORIG_SYS_LINE_REF' => $externalItemId,
            'ORDER_QTY' => round($rmaItem->getQtyAuthorized(),2),
            'PRIMARY_UOM_CODE' => $uomValue,
            'CUSTOMER_UNIT_PRICE' => $item->getPrice(),
            'ITEM_NO' => $modelCode,
            'SHIP_FROM_ORG' => $wareHouseCode,
            'NET_AMT' => $item->getPrice(),
            'ORIGINAL_LINE_ID' => $item->getData('gerp_line_id'),
            'RETURN_REASON_CODE' => (isset($reasonCode[$rmaItem->getCondition()]))?$reasonCode[$rmaItem->getCondition()]['value']:$rmaItem->getCondition(),
            'EDI_SENDER_ID' => $gerpWebsiteData['obs_id'],
            'EDI_RECEIVER_ID' => $gerpWebsiteData['gerp_id'],
        ];
    }

}