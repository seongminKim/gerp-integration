<?php

namespace Eguana\GERP\Model\Integration;


use Eguana\GERP\Model\Source\Status;

abstract class AbstractReceiver implements ReceiverInterface
{
    protected $_flagKey = '';

    protected $_receiveCollection;

    protected $_receiveCollectionObject;

    protected $_dataHelper;

    protected $_processingWebsite;

    public function __construct(
        \Eguana\GERP\Model\ResourceModel\Log\Collection $logCollection,
        \Eguana\GERP\Helper\Data $dataHelper
    )
    {
        $this->_receiveCollectionObject = $logCollection;
        $this->_dataHelper = $dataHelper;
    }

    public function execute(){

        if(!$this->getWebsiteCode()){
            return;
        }

        $bindData = $this->_bindData();

        if(count($bindData) == 0)
            return;

        $result = $this->process($bindData);

        $this->_result($result);
    }

    public function getDataHelper(){
        return $this->_dataHelper;
    }

    public function setWebsiteCode($websiteCode){
        $this->_processingWebsite = $websiteCode;
        return $this;
    }

    public function getWebsiteCode(){
        return $this->_processingWebsite;
    }

    /**
     * @return array
     */
    protected function _bindData(){

        $collection = $this->_getCollection();
        $bindData = [];

        foreach ($collection->getItems() as $item){
            $bindData[$item->getId()] = $item->getData();
            $bindData[$item->getId()][self::GERP_DATA_COLUMN] = json_decode($item->getData(self::GERP_DATA_COLUMN),true);
            $bindData[$item->getId()]['result'] = Status::GERP_DATA_ERRPR;
            $bindData[$item->getId()]['message'] = null;
        }

        return $bindData;
    }

    /**
     * @return \Eguana\GERP\Model\ResourceModel\Log\Collection
     */
    protected function _getCollection(){

        if(is_null($this->_receiveCollection)){
            $this->_receiveCollection = $this->_receiveCollectionObject->addFieldToFilter('gerp_flag',['eq' => $this->_flagKey])
                ->addFieldToFilter('status',['in' => [
                    \Eguana\GERP\Model\Source\Status::GERP_DEFAULT_DATA_RECEIVE,
                    \Eguana\GERP\Model\Source\Status::GERP_DATA_PENDING,
                ]])
                ->addFieldToFilter('website_code', ['eq' => $this->getWebsiteCode()]);
        }

        return $this->_receiveCollection;
    }

    protected function _result($resultData)
    {
        $resultIds = [];

        foreach ($resultData as $logID => $item){

            if($item['result'] == Status::GERP_DATA_PROCESS)
                $resultIds[Status::GERP_DATA_PROCESS][] = $logID;
            else
                $resultIds[$item['result']][] = [
                    'id' => $logID,
                    'message' => $item['message']
                ];
        }

        foreach ($resultIds as $key => $data){

            if($key == Status::GERP_DATA_PROCESS)
                $this->_statusUpdate($key,$data);
            else{
                $this->_statusRowUpdate($key,$data);
            }
        }

        $this->result($resultData);
    }

    protected function _statusRowUpdate($status,$data){

        $resource = $this->_getCollection()->getResource();
        $connection = $resource->getConnection();

        foreach ($data as $row){
            $connection->update($resource->getMainTable(),[
                'status' => $status,
                'message' => $row['message']
            ],[
                'entity_id = ?' => $row['id']
            ]);
        }

    }

    /**
     * @param $status string
     * @param $ids array
     * @return void
     */
    protected function _statusUpdate($status,$ids,$message = 'SUCCESS'){

        $resource = $this->_getCollection()->getResource();
        $connection = $resource->getConnection();

        $connection->update($resource->getMainTable(),[
            'status' => $status,
            'message' => $message
        ],[
            'entity_id IN (?)' => $ids
        ]);

    }

    public function getDataWebsiteScope($data){
        $websiteData = $this->getDataHelper()->getGerpWebsiteData();
        return $websiteData["$data->AFFILIATE_CODE"]['website_code'];
    }

}