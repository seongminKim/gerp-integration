<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-11
 * Time: 오후 2:43
 */

namespace Eguana\GERP\Model\Integration\Product;


use Amasty\MultiInventory\Model\Warehouse;
use Amasty\MultiInventory\Model\Warehouse\ItemRepository;
use Amasty\MultiInventory\Model\WarehouseFactory;
use Amasty\MultiInventory\Model\WarehouseRepository;
use Eguana\GERP\Model\Integration\AbstractReceiver;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductRepository;
use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Customer\Model\CustomerGroupConfig;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\App\ResourceConnection;

abstract class AbstractStock extends AbstractReceiver
{
    protected $_flagKey = '03';

    protected $_productRepository;

    protected $_warehouseRepository;

    protected $_warehouseFactory;

    protected $_itemRepository;

    protected $_customerGroupRepository;

    protected $_searchCriteriaBuilder;

    protected $_resourceConnection;

    public function __construct(
        \Eguana\GERP\Model\ResourceModel\Log\Collection $logCollection,
        \Eguana\GERP\Helper\Data $dataHelper,
        ProductRepository $productRepository,
        WarehouseFactory $warehouseFactory,
        WarehouseRepository $warehouseRepository,
        ItemRepository $itemRepository,
        GroupRepositoryInterface $customerGroupRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ResourceConnection $resourceConnection
    )
    {
        parent::__construct($logCollection,$dataHelper);
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_productRepository = $productRepository;
        $this->_warehouseRepository = $warehouseRepository;
        $this->_warehouseFactory = $warehouseFactory;
        $this->_itemRepository = $itemRepository;
        $this->_customerGroupRepository = $customerGroupRepository;
        $this->_resourceConnection = $resourceConnection;
    }

    protected function _saveWarehouseItem($warehouseItem){

        try{
            $this->_itemRepository->save($warehouseItem);
        }catch (CouldNotSaveException $e){
            return $e->getMessage();
        }

        return true;
    }

    protected function _getWarehouseItem($productId,$warehouseId){
        return $this->_itemRepository->getByProductWarehouse($productId,$warehouseId);
    }

    protected function _newWarehouse($title, $code, $description){

        $websiteConfig = $this->getDataHelper()->getGerpWebsiteData();

        /** @var $warehouse Warehouse */
        $warehouse = $this->_warehouseFactory->create();
        $warehouse->setCode($code);
        $warehouse->setTitle($title);
        $warehouse->setDescription($description);

        $customerGroupIds = [];
        foreach ($this->_customerGroupRepository->getList($this->_searchCriteriaBuilder->create())->getItems() as $item){
            $customerGroupIds[] = $item->getIdgi();
        }
        $warehouse->setCustomerGroups($customerGroupIds);

        try{
            return $this->_warehouseRepository->save($warehouse);
        }catch (CouldNotSaveException $e){
            return $warehouse;
        }

    }

    /**
     * @param $sku string
     * @return Product | false
     */
    protected function _getProduct($sku){
        
        try{
            $product = $this->_productRepository->get($sku, true, 2);
        }catch (\Exception $e){
            return false;
        }

        return $product;
    }

    public function result($resultData)
    {
        // TODO: Implement result() method.
    }

}