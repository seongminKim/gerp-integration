<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-18
 * Time: 오전 11:28
 */

namespace Eguana\GERP\Model\Integration;


interface SenderInterface
{

    /**
     * @return []
     */
    public function bindData();

    /**
     * @param $data []
     * @return []
     */
    public function process($data);

    /**
     * @param $data []
     * @return []
     */
    public function result($data);
}