<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\GERP\Model\Source;

class Status extends AbstractGerp
{
    const GERP_DEFAULT_DATA_RECEIVE = '01';

    const GERP_DATA_PROCESS = '02';

    const GERP_DATA_ERRPR = '03';

    const GERP_DATA_SKIP = '04';

    const GERP_DATA_PENDING = '05';

    const GERP_DATA_SENT = '11';

    const GERP_DATA_CANCEL = '15';

    const GERP_ORDER_ENTERED = '20';

    const GERP_ORDER_BOOCKED = '25';

    const GERP_ORDER_BACK = '30';

    const GERP_ORDER_BACKORERED = '31';

    const GERP_ORDER_HOLD = '35';

    const GERP_ORDER_OPEN = '40';

    const GERP_ORDER_AWAITING_SHIPPING = '41';

    const GERP_ORDER_CANCELLED = '42';

    const GERP_ORDER_CANCELED = '43';

    const GERP_ORDER_PICK = '45';

    const GERP_ORDER_DELY = '50';

    const GERP_ORDER_INVO = '55';

    const GERP_RETURN_REJECT = '60';

    const GERP_RETURN_BOOKED = '65';

    const GERP_RETURN_AWAITING_RETURN = '70';

    const GERP_RETURN_CLOSED = '75';

    const GERP_RETURN_INVO = '80';


    /**
     * Get values
     *
     * @return array
     */
    protected function _getValues()
    {
        return [
            self::GERP_DEFAULT_DATA_RECEIVE => 'RECEIVED',
            self::GERP_DATA_PROCESS => 'PROCESS',
            self::GERP_DATA_ERRPR => 'ERROR',
            self::GERP_DATA_SKIP => 'SKIP',
            self::GERP_DATA_PENDING => 'PENDING',
            self::GERP_DATA_SENT => 'SEND',
            self::GERP_DATA_CANCEL => 'CANCEL',
            self::GERP_ORDER_ENTERED => 'ENTERED',
            self::GERP_ORDER_BOOCKED => 'BOOKED',
            self::GERP_ORDER_BACK => 'BACK',
            self::GERP_ORDER_BACKORERED => 'BACKORERED',
            self::GERP_ORDER_HOLD => 'HOLD',
            self::GERP_ORDER_OPEN => 'OPEN',
            self::GERP_ORDER_PICK => 'PICK',
            self::GERP_ORDER_DELY => 'DELY',
            self::GERP_ORDER_INVO => 'INVO',
            self::GERP_RETURN_REJECT => 'GERP REJECT',
            self::GERP_RETURN_BOOKED => 'BOOKED',
            self::GERP_ORDER_AWAITING_SHIPPING => 'AWAITING_SHIPPING',
            self::GERP_RETURN_AWAITING_RETURN => 'AWAITING_RETURN',
            self::GERP_RETURN_CLOSED => 'CLOSED',
            self::GERP_RETURN_INVO => 'INVO',
            self::GERP_ORDER_CANCELLED => 'CANCELLED',
            self::GERP_ORDER_CANCELED => 'CANCELED'
        ];
    }
}
