<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\GERP\Model\Source;

use Eguana\GERP\Helper\Data;

abstract class AbstractGerp extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    protected $_dataHelper;

    public function __construct(
        Data $dataHelper
    )
    {
        $this->_dataHelper = $dataHelper;
    }

    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $result = [];
        foreach ($this->_getValues() as $k => $v) {
            $result[] = ['value' => $k, 'label' => $v];
        }

        return $result;
    }

    /**
     * Get option text
     *
     * @param int|string $value
     * @return null|string
     */
    public function getOptionText($value)
    {
        $options = $this->_getValues();
        if (isset($options[$value])) {
            return $options[$value];
        }
        return null;
    }

    /**
     * Get values
     *
     * @return array
     */
    public function getValues()
    {
        return $this->_getValues();
    }

    /**
     * Get values
     *
     * @return array
     */
    protected function _getValues()
    {
        $array = [];

        foreach ($this->_dataHelper->getGerpInterfaceConfig() as $list){
            $array[$list['flag']] = $list['document_type'];
        }

        return $array;
    }
}
