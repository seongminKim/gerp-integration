<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\GERP\Model\Source;

/**
 * @api
 * @since 100.0.2
 */
class YNFlag implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 'Y', 'label' => __('Yes')], ['value' => 'N', 'label' => __('No')]];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return ['N' => __('No'), 'Y' => __('Yes')];
    }
}
