<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\GERP\Model\Source;

/**
 * RMA Item status attribute model
 */
class RmaStatus extends \Magento\Rma\Model\Rma\Source\Status
{
    const STATE_RETURN_PROCESSING = 'return_processing';

    const STATE_RETURN_GERP_REJECT = 'gerp_reject';

    /**
     * Get state label based on the code
     *
     * @param string $state
     * @return \Magento\Framework\Phrase|string
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function getItemLabel($state)
    {
        switch ($state) {
            case self::STATE_PENDING:
                return __('Zgłoszony zwrot');  //Return Requested
            case self::STATE_AUTHORIZED:
                return __('Zwrot zaakceptowany'); //Return Authorized
            case self::STATE_RETURN_PROCESSING:
                return __('Przetwarzanie zwrotu'); //Return Processing
            case self::STATE_PARTIAL_AUTHORIZED:
                return __('Partially Authorized'); //Partially Authorized
            case self::STATE_RECEIVED:
                return __('Zwrot zakończony'); //Return Completed
            case self::STATE_RECEIVED_ON_ITEM:
                return __('Return Partially Received'); //Return Partially Received
            case self::STATE_APPROVED:
                return __('Approved'); //Approved
            case self::STATE_APPROVED_ON_ITEM:
                return __('Partially Approved'); //Partially Approved
            case self::STATE_REJECTED:
                return __('Rejected'); //Rejected
            case self::STATE_REJECTED_ON_ITEM:
                return __('Partially Rejected'); //Partially Rejected
            case self::STATE_DENIED:
                return __('Denied'); //Denied
            case self::STATE_CLOSED:
                return __('Return Refund'); //Return Refund
            case self::STATE_PROCESSED_CLOSED:
                return __('Zwrot kosztów zakończony'); //Refund completed
            case self::STATE_RETURN_GERP_REJECT:
                return __('GERP Reject'); //GERP Reject
            default:
                return $state;
        }
    }
    /**
     * Get available states keys for entities
     *
     * @return string[]
     */
    protected function _getAvailableValues()
    {
        return [
            self::STATE_PENDING,
            self::STATE_AUTHORIZED,
            self::STATE_RETURN_PROCESSING,
            self::STATE_RETURN_GERP_REJECT,
            self::STATE_PARTIAL_AUTHORIZED,
            self::STATE_RECEIVED,
            self::STATE_RECEIVED_ON_ITEM,
            self::STATE_APPROVED_ON_ITEM,
            self::STATE_REJECTED_ON_ITEM,
            self::STATE_CLOSED,
            self::STATE_PROCESSED_CLOSED
        ];
    }
}
