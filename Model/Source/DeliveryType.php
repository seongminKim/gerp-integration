<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\GERP\Model\Source;

class DeliveryType extends AbstractGerp
{
    const DELIVERY_TYPE_NORMAL = 'R';

    const DELIVERY_TYPE_TRUCK = 'S';

    const DELIVERY_TYPE_PARCEL = 'P';

    const DELIVERY_TYPE_HOME_DELIVERY = 'H';

    const DELIVERY_TYPE_DIRECT_SHIPPING = 'D';

    const DELIVERY_TYPE_PICKUP = 'C';

    /**
     * Get values
     *
     * @return array
     */
    protected function _getValues()
    {
        return [
            self::DELIVERY_TYPE_NORMAL => 'Normal',
            self::DELIVERY_TYPE_TRUCK => 'Truck',
            self::DELIVERY_TYPE_PARCEL => 'Parcel',
            self::DELIVERY_TYPE_HOME_DELIVERY => 'Home Delivery',
            self::DELIVERY_TYPE_DIRECT_SHIPPING => 'Direct Shipping',
            self::DELIVERY_TYPE_PICKUP => 'Pickup',
        ];
    }
}
