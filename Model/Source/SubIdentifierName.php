<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eguana\GERP\Model\Source;

class SubIdentifierName extends AbstractGerp
{
    /**
     * Get values
     *
     * @return array
     */
    protected function _getValues()
    {
        $array = [];

        foreach ($this->_dataHelper->getGerpInterfaceConfig() as $list){
            $array[$list['sub_identifier_name']] = $list['sub_identifier_name'];
        }

        return $array;
    }
}
