<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-11-14
 * Time: 오전 10:29
 */

namespace Eguana\GERP\Cron\Order\OMV;


use Eguana\GERP\Helper\Data;
use Eguana\GERP\Model\Integration\Product\AbstractProduct;
use Eguana\GERP\Model\Source\Status;
use Magento\Framework\App\ResourceConnection;
use Magento\Sales\Model\Order\ItemRepository;
use Magento\Sales\Model\OrderRepository;

class ItemComplete
{
    protected $_orderItemCollection;

    protected $_orderRepository;

    protected $_orderData;

    protected $_itemData;

    protected $_dataHelper;

    protected $_itemRepository;

    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\Item\Collection $orderItemCollection,
        OrderRepository $orderRepository,
        ItemRepository $itemRepository,
        Data $dataHelper
    )
    {
        $this->_orderItemCollection = $orderItemCollection;
        $this->_orderRepository = $orderRepository;
        $this->_itemRepository = $itemRepository;
        $this->_dataHelper = $dataHelper;
    }

    public function execute(){

        $updateItems = $this->_completePendingItemCollection();

        /** @var $item \Magento\Sales\Model\Order\Item */
        foreach ($updateItems as $item){
            $order = $this->_getOrder($item->getOrderId());
            $item = $this->_getItem($item->getItemId());
            $item->setData('interface_status',Status::GERP_ORDER_INVO);
            $statusUpdate = true;

            foreach ($order->getAllItems() as $orderItem){

                if($item->getProductType() == 'configurable')
                    continue;

                if($orderItem->getItemId() == $item->getItemId()){
                    continue;
                }

                if(!in_array($orderItem->getData('interface_status'),[
                    Status::GERP_ORDER_INVO
                ])){
                    $statusUpdate = false;
                }
            }

            if($statusUpdate)
                $order->setStatus('delivery_completed');

        }


        if(count($this->_orderData) > 0){
            foreach ($this->_orderData as $order){
                $this->_orderRepository->save($order);
            }
        }
        if(count($this->_orderData) > 0){
            foreach ($this->_itemData as $updateItem){
                $this->_itemRepository->save($updateItem);
            }
        }

    }

    protected function _getItem($itemId){
        if(!isset($this->_itemData[$itemId])){
            $this->_itemData[$itemId] = $this->_itemRepository->get($itemId);
        }
        return $this->_itemData[$itemId];
    }

    public function getCompleteChangeDay(){
        return $this->_dataHelper->getGerpGeneralConfig('omv_complete_change_cron');
    }

    /** @return \Magento\Sales\Model\Order */
    protected function _getOrder($orderId){

        if(!isset($this->_orderData[$orderId])){
            $this->_orderData[$orderId] = $this->_orderRepository->get($orderId);
        }

        return $this->_orderData[$orderId];
    }

    protected function _completePendingItemCollection(){
        $day = $this->getCompleteChangeDay();
        if($day > 0){
            $updateDate = date('Y-m-d H:i:s',strtotime(sprintf("-%d day",$day)));
        }else{
            $updateDate = date('Y-m-d H:i:s');
        }
        return $this->_orderItemCollection
            ->addFieldToFilter('product_from',['eq' => AbstractProduct::OMV_PRODUCT_FROM])
            ->addFieldToFilter('updated_at',['lteq' => $updateDate])
            ->addFieldToFilter('interface_status',['eq' => Status::GERP_RETURN_CLOSED]);
    }

}