<?php
/**
 * Created by ${Eguana}.
 * User: ${glenn}
 * Date: 2019-02-13
 * Time: 오전 9:17
 */

namespace Eguana\GERP\Cron\Product\OMD;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Catalog\Model\Product;
use Magento\Store\Model\StoreManagerInterface;

class PriceDate
{
    protected $_resource;

    protected $_productRepository;

    protected $_storeManager;

    protected $_action;

    public function __construct(
        ResourceConnection $productResource,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ResourceModel\Product\Action $action
    )
    {
        $this->_resource = $productResource;
        $this->_productRepository = $productRepository;
        $this->_storeManager = $storeManager;
        $this->_action = $action;
    }

    public function execute()
    {
        $resource = $this->_resource;
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('eguana_gerp_price_date');
        $date = date('Y-m-d H:i:s');

        $sql = "Select * FROM " . $tableName. " WHERE apply_date <='$date' AND expiration_date > '$date' AND status = 'N'";
        $result = $connection->fetchAll($sql);

        if($result)
        {
            foreach ($result as $data)
            {
                $this->updatePrice($data,$connection,$tableName);
            }
        }

        $select = "SELECT * FROM $tableName WHERE apply_date < '$date' AND expiration_date < '$date' AND status='N'";
        $selectData  = $connection->fetchAll($select);

        if($selectData)
        {
            foreach ($selectData as $data)
            {
                $this->updateSkip($data,$connection,$tableName);
            }
        }
    }

    protected function updatePrice($data,$connection,$tableName)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . sprintf('/var/log/gerp_price_date.%s.log',date('y-m-d')));
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        $sku = $data['sku'];
        $entity_id = $data['entity_id'];
        $product = $this->_getProduct($sku);

        if(!$product){
            $logger->info('product does not exist');
        }

        $product->setWebsiteIds([2]);
        $product->setStoreId($this->_storeManager->getWebsite(2)->getId());

        try{
            $this->_action->updateAttributes([$product->getId()],['special_price'=>$data['unit_price']],2);
            $this->_action->updateAttributes([$product->getId()],['price'=>$data['rrp_price']],0);
            $this->_action->updateAttributes([$product->getId()],['transferred'=>0],2);
            $result = true;
        }catch (\Exception $e){
            $logger->info($data['sku'].' price is not updated');
        }

        if($result == true)
        {
            $logger->info($data['sku'].' price is updated');
            $logger->info('price ='.$data['rrp_price']. ' / special price ='.$data['unit_price']);
            $updateStatus = "Update " . $tableName . " Set status = 'Y' where entity_id = $entity_id";
            $connection->query($updateStatus);
        }else{
            $logger->info('error during saving product');
            $updateStatus = "Update " . $tableName . " Set status = 'E' where entity_id = $entity_id";
            $connection->query($updateStatus);
        }
    }

    protected function updateSkip($data,$connection,$tableName)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . sprintf('/var/log/gerp_price_date.%s.log',date('y-m-d')));
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        $entity_id = $data['entity_id'];

        $logger->info('SKIP data');
        $updateStatus = "Update " . $tableName . " Set status = 'S' where entity_id = $entity_id";
        $connection->query($updateStatus);
    }


    /**
     * @param $sku string
     * @return Product | false
     */
    protected function _getProduct($sku){

        try{
            $product = $this->_productRepository->get($sku, false, 2);
        }catch (NoSuchEntityException $e){
            return false;
        }

        return $product;
    }

    /**
     * @param $product Product
     * @return boolean | string
     */
    protected function _saveProduct($product){

        $result = true;

        try{
            $product->save();
        }catch (\Exception $e){
            return $e->getMessage();
        }

        return $result;
    }
}
