<?php
/**
 * Created by ${Eguana}.
 * User: glenn
 * Date: 2019-01-28
 * Time: 오전 9:43
 */

namespace Eguana\GERP\Cron\Product;

use Magento\Framework\App\ResourceConnection;

class Delete
{
    protected $_resource;

    protected $_scopeConfig;

    public function __construct(
        ResourceConnection $productResource,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->_resource = $productResource;
        $this->_scopeConfig = $scopeConfig;
    }

    public function execute() {
        $writer = new \Zend\Log\Writer\Stream(BP . sprintf('/var/log/gerp_delete.%s.log',date('y-m-d')));
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(__METHOD__);

        $days = $this->_scopeConfig->getValue('gerp/general/product_delete_days');

        $resourceCon = $this->_resource;
        $connection = $resourceCon->getConnection();
        $table = $resourceCon->getTableName('eguana_gerp_log');

        $timestamp = strtotime("-".$days. "days");
        $deleteDay = date('Y-m-d H:i:s',$timestamp);
        $deleteFlag = [1,2,3,4,5,6];

        $where = [
            'creation_time <= ?' => $deleteDay,
            'gerp_flag IN (?)' => $deleteFlag,
        ];


        $logger->info('Order Deleted Before'.$deleteDay);

        $result = $connection->delete($table, $where);

        if($result === true)
        {
            $logger->info('delete success');
        }else{
            $logger->info('No data to delete');
        }

    }
}