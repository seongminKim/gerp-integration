<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2017-12-12
 * Time: 오후 6:48
 */
namespace Eguana\GERP\Setup;

use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Config;

class InstallData implements InstallDataInterface
{
    /** @var \Magento\Catalog\Setup\CategorySetupFactory */
    protected $_categorySetupFactory;

    /** @var Config */
    protected $_eavConfig;

    /** @var \Magento\Customer\Model\GroupFactory */
    protected $_customerGroupFactory;

    public function __construct(
        CategorySetupFactory $categorySetupFactory,
        Config $eavConfig
    )
    {
        $this->_categorySetupFactory = $categorySetupFactory;
        $this->_eavConfig = $eavConfig;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {

        $addAddAttribute = $this->addAttributeData();
        $this->_addProductAttribute($setup,$addAddAttribute);
        $this->_orderStatusSettings($setup);

    }

    protected function _addProductAttribute(ModuleDataSetupInterface $setup,$addAddAttribute){

        /** @var \Magento\Catalog\Setup\CategorySetup $categorySetup */
        $categorySetup = $this->_categorySetupFactory->create(['setup' => $setup]);

        foreach ($addAddAttribute as $code => $data){

            $categorySetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                $code,
                $data
            );
        }

    }

    protected function _orderStatusSettings(ModuleDataSetupInterface $setup){

        /**
         * Install order statuses from config
         */
        $data = [];
        $statuses = [
            'order_processing' => __('Order Processing'),
            'preparing_for_delivery' => __('Preparing for Delivery'),
            'picking_for_delivery' => __('Picking for Delivery'),
            'on_delivery' => __('On Delivery'),
            'delivery_completed' => __('Delivery Completed'),
        ];

        foreach ($statuses as $code => $info) {
            $data[] = ['status' => $code, 'label' => $info];
        }

        $setup->getConnection()->insertArray($setup->getTable('sales_order_status'), ['status', 'label'], $data);


        /**
         * Install order states from config
         */
        $data = [];
        $states = [
            [
                'status' => 'order_processing',
                'state' => 'processing',
                'label' => __('Order Processing'),
                'is_default' => false,
                'visible_on_front' => true,
            ],
            [
                'status' => 'preparing_for_delivery',
                'state' => 'processing',
                'label' => __('Preparing for Delivery'),
                'is_default' => false,
                'visible_on_front' => true,
            ],
            [
                'status' => 'picking_for_delivery',
                'state' => 'processing',
                'label' => __('Picking for Delivery'),
                'is_default' => false,
                'visible_on_front' => true,
            ],
            [
                'status' => 'on_delivery',
                'state' => 'processing',
                'label' => __('On Delivery'),
                'is_default' => false,
                'visible_on_front' => true,
            ],
            [
                'status' => 'delivery_completed',
                'state' => 'complete',
                'label' => __('Delivery Completed'),
                'is_default' => false,
                'visible_on_front' => true,
            ],
        ];

        foreach ($states as $info) {
            $data[] = [
                'status' => $info['status'],
                'state' => $info['state'],
                'is_default' => $info['is_default'],
                'visible_on_front' => $info['visible_on_front']
            ];
        }


        $setup->getConnection()->insertArray(
            $setup->getTable('sales_order_status_state'),
            ['status', 'state', 'is_default','visible_on_front'],
            $data
        );

        $setup->getConnection()->update($setup->getTable('sales_order_status'),['label' => 'Place Order'],
            [
                'status=?' => 'processing'
            ]);


        $setup->getConnection()->update($setup->getTable('sales_order_status'),['label' => 'Pre-Completed'],
            [
                'status=?' => 'complete'
            ]);


        $setup->getConnection()->update($setup->getTable('sales_order_status'),['label' => 'Return Refund'],
            [
                'status=?' => 'closed'
            ]);
    }

    /** @return array */
    private function addAttributeData(){

        return [
            'product_level4_code' => [
                'attribute_code' => 'product_level4_code',
                'input' => 'text',
                'type' => 'varchar',
                'group' => 'LG Attributes',
                'sort_order' => 5,
                'label' => 'Product Level4 Code',
                'required' => false,
            ],
            'product_group1_name' => [
                'attribute_code' => 'product_group1_name',
                'input' => 'text',
                'type' => 'varchar',
                'group' => 'LG Attributes',
                'sort_order' => 5,
                'label' => 'Product Group1 Name',
                'required' => false,
            ],
            'product_group2_name' => [
                'attribute_code' => 'product_group2_name',
                'input' => 'text',
                'type' => 'varchar',
                'group' => 'LG Attributes',
                'sort_order' => 5,
                'label' => 'Product Group2 Name',
                'required' => false,
            ],
            'product_group3_name' => [
                'attribute_code' => 'product_group3_name',
                'input' => 'text',
                'type' => 'varchar',
                'group' => 'LG Attributes',
                'sort_order' => 5,
                'label' => 'Product Group3 Name',
                'required' => false,
            ],
            'product_group4_name' => [
                'attribute_code' => 'product_group4_name',
                'input' => 'text',
                'type' => 'varchar',
                'group' => 'LG Attributes',
                'sort_order' => 5,
                'label' => 'Product Group4 Name',
                'required' => false,
            ],
            'primary_uom_code' => [
                'attribute_code' => 'primary_uom_code',
                'input' => 'text',
                'type' => 'varchar',
                'group' => 'LG Attributes',
                'sort_order' => 5,
                'label' => 'PRIMARY UOM CODE',
                'required' => false,
            ],
            'gerp_item_type' => [
                'attribute_code' => 'gerp_item_type',
                'input' => 'select',
                'type' => 'varchar',
                'sort_order' => 5,
                'label' => 'Gerp Item Type',
                'required' => false,
                'group' => 'LG Attributes',
                'source' => 'Eguana\GERP\Model\Source\ItemType',
            ],
            'delivery_type' => [
                'attribute_code' => 'delivery_type',
                'input' => 'select',
                'type' => 'varchar',
                'sort_order' => 5,
                'label' => 'Delivery Type',
                'required' => true,
                'group' => 'LG Attributes',
                'source' => 'Eguana\GERP\Model\Source\DeliveryType',
            ],
            'product_carrier' => [
                'attribute_code' => 'product_carrier',
                'input' => 'select',
                'type' => 'varchar',
                'sort_order' => 5,
                'label' => 'Product Carrier',
                'required' => true,
                'group' => 'LG Attributes',
                'source' => 'Eguana\GERP\Model\Source\ProductCarrier',
            ],
            'product_from' => [
                'attribute_code' => 'product_from',
                'input' => 'text',
                'type' => 'varchar',
                'sort_order' => 5,
                'label' => 'Product From',
                'required' => true,
                'group' => 'LG Attributes',
            ]
        ];
    }
}
