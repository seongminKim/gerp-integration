<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-08-27
 * Time: 오전 10:33
 */

namespace Eguana\GERP\Setup;

use Eguana\GERP\Model\Source\Status;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class InstallSchema implements InstallSchemaInterface
{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $this->_gerpGerpLogTableCreate($setup);
        $this->_orderTableColumnAdd($setup);
        $this->_orderGridTableColumnAdd($setup);
        $this->_orderItemTableColumnAdd($setup);
        $this->_quoteItemTableColumnAdd($setup);
        $this->_rmaItemTableColumnAdd($setup);

        $setup->endSetup();
    }

    protected function _gerpGerpLogTableCreate(SchemaSetupInterface $setup){


        /**
         * Create table 'eguana_gerp_receive'
         */
        $gerpReceiveTable = $setup->getConnection()->newTable($setup->getTable('eguana_gerp_log'))
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )->addColumn(
                'gerp_flag',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                2,
                ['nullable' => false],
                'GERP FLAG'
            )->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                2,
                [
                    'nullable' => false,
                    'default'   => '01'
                ],
                'GERP Log Status'
            )->addColumn(
                'identifier_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                100,
                ['nullable' => false],
                'KEY NAME'
            )->addColumn(
                'identifier',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                100,
                ['nullable' => false],
                'KEY'
            )->addColumn(
                'sub_identifier_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                100,
                ['nullable' => false],
                'SUB KEY NAME'
            )->addColumn(
                'sub_identifier',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                100,
                ['nullable' => false],
                'SUB KEY'
            )->addColumn(
                'gerp_data',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '2M',
                ['nullable' => false],
                'GERP DATA'
            )->addColumn(
                'message',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'MESSAGE'
            )->addColumn(
                'website_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => false],
                'Website Code'
            )->addColumn(
                'creation_time',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Log Creation Time'
            )->addColumn(
                'interface_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                100,
                ['nullable' => true],
                'Interface ID'
            )->addColumn(
                'sender_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                20,
                ['nullable' => true],
                'Sender ID'
            )->addColumn(
                'receiver_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                20,
                ['nullable' => true],
                'Receiver ID'
            )->addColumn(
                'update_time',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Log Modification Time'
            )->addIndex(
                $setup->getIdxName(
                    $setup->getTable('eguana_gerp_log'),
                    ['gerp_flag','status']
                ),['gerp_flag','status']
            );

        $setup->getConnection()->createTable($gerpReceiveTable);
    }

    protected function _orderTableColumnAdd(SchemaSetupInterface $setup){

        /**
         * @var $installer \Magento\Framework\Setup\SchemaSetupInterface
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),'omv_header_id',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 100,
                'nullable' => true,
                'comment' => 'GERP Omv Header ID'
            ]
        );


        /**
         * @var $installer \Magento\Framework\Setup\SchemaSetupInterface
         */
        $setup->getConnection()
            ->addColumn(
                $setup->getTable('sales_order'),'gerp_header_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 100,
                    'nullable' => true,
                    'comment' => 'GERP Header ID'
                ]
            );

    }

    protected function _orderGridTableColumnAdd(SchemaSetupInterface $setup){

        /**
         * @var $installer \Magento\Framework\Setup\SchemaSetupInterface
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_grid'),'gerp_header_id',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 100,
                'nullable' => true,
                'comment' => 'GERP Header ID'
            ]
        );

        /**
         * @var $installer \Magento\Framework\Setup\SchemaSetupInterface
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_grid'),'omv_header_id',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 100,
                'nullable' => true,
                'comment' => 'GERP Omv Header ID'
            ]
        );

    }

    protected function _orderItemTableColumnAdd(SchemaSetupInterface $setup){

        /**
         * @var $installer \Magento\Framework\Setup\SchemaSetupInterface
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_item'),'interface_status',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 2,
                'nullable' => true,
                'default' => Status::GERP_DATA_PENDING,
                'comment' => 'Interface Status'
            ]
        );

        $setup->getConnection()
            ->addColumn(
                $setup->getTable('sales_order_item'),'delivery_type',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 2,
                    'nullable' => true,
                    'comment' => 'Product From'
                ]
            );

        /**
         * @var $installer \Magento\Framework\Setup\SchemaSetupInterface
         */
        $setup->getConnection()
            ->addColumn(
                $setup->getTable('sales_order_item'),'product_from',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 30,
                    'nullable' => true,
                    'comment' => 'Product From'
                ]
            );

        /**
         * @var $installer \Magento\Framework\Setup\SchemaSetupInterface
         */
        $setup->getConnection()
            ->addColumn(
                $setup->getTable('sales_order_item'),'product_carrier',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 30,
                    'nullable' => true,
                    'comment' => 'Product Carrier'
                ]
            );

        /**
         * @var $installer \Magento\Framework\Setup\SchemaSetupInterface
         */
        $setup->getConnection()
            ->addColumn(
                $setup->getTable('sales_order_item'),'gerp_line_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 100,
                    'nullable' => true,
                    'comment' => 'GERP LINE ID'
                ]
            );

        /**
         * @var $installer \Magento\Framework\Setup\SchemaSetupInterface
         */
        $setup->getConnection()
            ->addColumn(
                $setup->getTable('sales_order_item'),'erp_order_no',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 100,
                    'nullable' => true,
                    'comment' => 'ERP Order No'
                ]
            );

        $setup->getConnection()
            ->addColumn(
                $setup->getTable('sales_order_item'),'back_interface',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 1,
                    'nullable' => true,
                    'comment' => 'Back Interface'
                ]
            );
    }

    protected function _quoteItemTableColumnAdd(SchemaSetupInterface $setup){

        $setup->getConnection()
            ->addColumn(
                $setup->getTable('quote_item'),'delivery_type',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 2,
                    'nullable' => true,
                    'comment' => 'Product From'
                ]
            );

        $setup->getConnection()
            ->addColumn(
                $setup->getTable('quote_item'),'product_from',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 30,
                    'nullable' => true,
                    'comment' => 'Product From'
                ]
            );

        $setup->getConnection()
            ->addColumn(
                $setup->getTable('quote_item'),'product_carrier',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 30,
                    'nullable' => true,
                    'comment' => 'Product Carrier'
                ]
            );

    }

    protected function _rmaItemTableColumnAdd(SchemaSetupInterface $setup){

        $setup->getConnection()
            ->addColumn(
                $setup->getTable('magento_rma_item_entity'),'interface_status',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 2,
                    'nullable' => true,
                    'comment' => 'Interface Interface'
                ]
            );

        $setup->getConnection()
            ->addColumn(
                $setup->getTable('magento_rma_item_entity'),'back_interface',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 1,
                    'nullable' => true,
                    'comment' => 'Back Interface'
                ]
            );
    }
}