<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-08
 * Time: 오후 5:23
 */

namespace Eguana\GERP\Api\Data;


interface LogInterface
{
    const ENTITY_ID = 'entity_id';

    const GERP_FLAG = 'gerp_flag';

    const STATUS = 'status';

    const IDENTIFIER_NAME = 'identifier_name';

    const IDENTIFIER = 'identifier';

    const SUB_IDENTIFIER_NAME = 'sub_identifier_name';

    const SUB_IDENTIFIER = 'sub_identifier';

    const GERP_DATA = 'gerp_data';

    const GERP_MESSAGE = 'message';

    const WEBSITE_CODE = 'website_code';

    const INTERFACE_ID = 'interface_id';

    const SENDER_ID = 'sender_id';

    const  RECEIVER_ID = 'receiver_id';

    /**
     * @return int
    */
    public function getEntityId();

    /**
     * @param $flag string
     * @return $this
    */
    public function setGERPFlag($flag);

    /** @return string*/
    public function getGERPFlag();

    /**
     * @param $status string
     * @return $this
     */
    public function setStatus($status);

    /** @return string*/
    public function getStatus();

    /**
     * @param $identifierName string
     * @return $this
     */
    public function setIdentifierName($identifierName);

    /** @return string*/
    public function getIdentifierName();

    /**
     * @param $identifier string
     * @return $this
     */
    public function setIdentifier($identifier);

    /** @return string*/
    public function getIdentifier();

    /**
     * @param $subIdentifierName string
     * @return $this
     */
    public function setSubIdentifierName($subIdentifierName);

    /** @return string*/
    public function getSubIdentifierName();

    /**
     * @param $subIdentifier string
     * @return $this
     */
    public function setSubIdentifier($subIdentifier);

    /** @return string*/
    public function getSubIdentifier();

    /**
     * @param $data string
     * @return $this
     */
    public function setGERPData($data);

    /** @return string*/
    public function getGERPData();

    /**
     * @param $message string
     * @return $this
     */
    public function setMessage($message);

    /** @return string*/
    public function getMessage();

    /**
     * @param $websiteCode string
     * @return $this
     */
    public function setWebsiteCode($websiteCode);

    /** @return string*/
    public function getWebsiteCode();

    /**
     * @param $interfaceId string
     * @return $this
     */
    public function setInterfaceId($interfaceId);

    /** @return string*/
    public function getInterfaceId();

    /**
     * @param $senderId string
     * @return $this
     */
    public function setSenderId($senderId);

    /** @return string*/
    public function getSenderId();

    /**
     * @param $receiverId string
     * @return $this
     */
    public function setReceiverId($receiverId);

    /** @return string*/
    public function getReceiverId();


}