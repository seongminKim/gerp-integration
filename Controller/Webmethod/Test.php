<?php
/**
 * Created by ${Eguana}.
 * User: ${Jane}
 * Date: 2019-02-26
 * Time: 오전 9:22
 */

namespace Eguana\GERP\Controller\Webmethod;



use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Test extends Action
{
    protected $_stock;

    protected $context;

    public function __construct(
        Context $context,
        \Eguana\GERP\Cron\Product\OMD\PriceDate $stockUpdate
    )
    {
        parent::__construct($context);
        $this->_stock = $stockUpdate;
    }

    public function execute()
    {
        $this->_stock->execute();
        // TODO: Implement execute() method.
    }

}