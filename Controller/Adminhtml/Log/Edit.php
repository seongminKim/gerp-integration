<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 2018-10-08
 * Time: 오후 5:10
 */

namespace Eguana\GERP\Controller\Adminhtml\Log;


use Eguana\GERP\Controller\Adminhtml\AbstractAction;
use Eguana\GERP\Model\Log;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class Edit extends AbstractAction
{
    protected $_coreRegistry;

    protected $_gerpLogModel;

    public function __construct(
        Context $context,
        \Magento\Framework\Registry $coreRegistry,
        Log $gerpLogModel
    )
    {
        $this->_coreRegistry = $coreRegistry;
        $this->_gerpLogModel = $gerpLogModel;
        parent::__construct($context);
    }

    public function execute()
    {

        $id = $this->getRequest()->getParam('entity_id');
        $model = $this->_gerpLogModel;

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This log no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $this->_coreRegistry->register('eguana_gerp_log', $model);


        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Log') : __('New Log'),
            $id ? __('Edit Log') : __('New Log')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Log'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? $model->getTitle() : __('New Log'));
        return $resultPage;

    }

}